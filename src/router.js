import Vue from 'vue';
import Router from 'vue-router';
import UserDashboard from "@/components/views/UserDashboard";

// import DaLatLayout from "@/layouts/DaLatLayout";
const LayoutDefault = () => import('./layouts/LayoutDefault');

const DalatOurChoice = () => import('./components/views/LargestPopular/DaLat/DaLatOurChoice');
const DaNangOurChoice = () => import('./components/views/LargestPopular/DaNang/DaNangOurChoice');
const HomeAndApartment = () => import('./components/views/LargestPopular/DaLat/Home&Apartment');
const HaNoiOurChoice = () => import('./components/views/LargestPopular/HaNoi/HaNoiOurChoice');
const HoChiMinhOurChoice = () => import('./components/views/LargestPopular/HoChiMinh/HoChiMinhOurChoice');
const PhuQuocOurChoice = () => import('./components/views/LargestPopular/PhuQuoc/PhuQuocOurChoice');
const Login = () => import('./components/views/ModalLogin');
const Register = () => import('./components/views/ModalRegister');
const Home = () => import(`./components/views/Home`);
const About = () => import(`./components/views/About.vue`);
// const User = () => import('./components/views/User');

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: "*",
            redirect: '/home',
            component: Home
        },
        {
            path: `/`,
            name: `home`,
            component: Home,
            meta: {layout: LayoutDefault},
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: {layout: LayoutDefault},
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
            meta: {layout: LayoutDefault},
        },
        {
            path: '/logout',
            name: 'logout',
            component: Home,
            meta: {layout: LayoutDefault},
        },
        {
            path: `/about`,
            name: `about`,
            component: About,
            meta: {layout: LayoutDefault},
        },
        {
            path: '/dalat',
            name: 'dalat',
            component: DalatOurChoice,
            meta: {layout: LayoutDefault},
            children: [
                {path:'home_apartment', components: HomeAndApartment},
            ],
        },
        {
            path: '/danang',
            name: 'danang',
            component: DaNangOurChoice,
            meta: {layout: LayoutDefault},
        },
        {
            path: '/hanoi',
            name: 'hanoi',
            component: HaNoiOurChoice,
            meta: {layout: LayoutDefault},
        },
        {
            path: '/hochiminh',
            name: 'hochiminh',
            component: HoChiMinhOurChoice,
            meta: {layout: LayoutDefault},
        },
        {
            path: '/phuquoc',
            name: 'phuquoc',
            component: PhuQuocOurChoice,
            meta: {layout: LayoutDefault},
        },
        {
            path: '/user',
            name: 'user',
            component: UserDashboard,
            meta: {layout: LayoutDefault},
            children: [
                // {path: 'message', components: UserMessage},
                // {path: 'comment', components: UserComment},
                // {path: 'vip', components: UserVip},
                // {path: 'money_pagoda', components: UserMoney},
                // {path: 'profile', components: UserProfile},
            ]
        }
    ],
    mode: 'history',
    base: process.env.BASE_URL,
});
